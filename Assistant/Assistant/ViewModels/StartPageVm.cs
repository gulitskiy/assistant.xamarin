﻿using Assistant.Models;
using Assistant.Services;
using Assistant.Views;
using Syncfusion.SfRotator.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Assistant.ViewModels {
    public class StartPageVm : BaseVm<StartPage> {
        public bool StateVisibility { get; set; } = true;
        public bool ListVisibility { get; set; } = false;
        AssistantService _assistantService;
        public ObservableCollection<SfRotatorItem> AdUnits { get; set; }
        public ObservableCollection<UserInfo> Cards { get; set; }

        public StartPageVm(StartPage view, AssistantService assistantService) : base(view) {
            _assistantService = assistantService;
            AdUnits = new ObservableCollection<SfRotatorItem>() {
                new SfRotatorItem() { Image = "ad1.jpg" },
                new SfRotatorItem() { Image = "ad2.jpg" },
                new SfRotatorItem() { Image = "ad3.jpg" }
            };
            Cards = new ObservableCollection<UserInfo>();
        }

        public override Task AfterAppearing() {
            StateVisibility = true;
            ListVisibility = false;
            OnPropertyChanged(nameof(StateVisibility));
            OnPropertyChanged(nameof(ListVisibility));
            return System.Threading.Tasks.Task.Factory.StartNew(async () => {
                try {
                    var res = await _assistantService.GetUserInfo();
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(() => {
                        Cards.Clear();
                        foreach (var item in res) {
                            Cards.Add(item);
                        }
                        StateVisibility = false;
                        ListVisibility = true;
                        OnPropertyChanged(nameof(StateVisibility));
                        OnPropertyChanged(nameof(ListVisibility));
                    });
                } catch (WebException ex) {
                    await AuthService.Logout();
                } catch (Exception ex) {

                }
            });
        }
    }
}
