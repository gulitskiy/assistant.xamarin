﻿using Assistant.Models;
using Assistant.Services;
using Assistant.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Assistant.ViewModels {
    public class AkgunHistoryPageVm : BaseVm<AkgunHistoryPage> {
        public bool StateVisibility { get; set; } = true;
        public bool ListVisibility { get; set; } = false;
        public bool IsEmpty { get; set; } = false;

        int _take = 15;
        int _offset = 0;

        AkgunService _akgunService;
        public ObservableCollection<AkgunHistory> History { get; set; }

        public AkgunHistoryPageVm(AkgunHistoryPage view, AkgunService akgunService)
            : base(view) {
            _akgunService = akgunService;
            History = new ObservableCollection<AkgunHistory>();
        }

        public override Task AfterAppearing() {
            StateVisibility = true;
            ListVisibility = false;
            OnPropertyChanged(nameof(StateVisibility));
            OnPropertyChanged(nameof(ListVisibility));
            return System.Threading.Tasks.Task.Factory.StartNew(async () => {
                try {
                    var res = await _akgunService.GetHistory();
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(() => {
                        History.Clear();
                        if(res != null) {
                            foreach (var item in res) {
                                History.Add(item);
                            }
                        }
                        StateVisibility = false;
                        ListVisibility = History.Count > 0;
                        IsEmpty = History.Count == 0;
                        OnPropertyChanged(nameof(StateVisibility));
                        OnPropertyChanged(nameof(ListVisibility));
                        OnPropertyChanged(nameof(IsEmpty));
                    });
                } catch (WebException ex) {
                    await AuthService.Logout();
                } catch (Exception ex) {

                }
            });
        }
    }
}
