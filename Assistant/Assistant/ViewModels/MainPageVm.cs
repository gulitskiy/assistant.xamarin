﻿using Assistant.Models;
using Assistant.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Assistant.ViewModels {
    public class MainPageVm : BaseVm<MainPage> {
        public ObservableCollection<MasterPageItem> MenuItems { get; set; }
        MasterPageItem _selectedMenuItem;
        public MasterPageItem SelectedMenuItem {
            get => _selectedMenuItem;
            set {
                _selectedMenuItem = value;
                OnPropertyChanged();
            }
        }

        public MainPageVm(MainPage view) : base(view) {
            MenuItems = new ObservableCollection<MasterPageItem>() {
                new MasterPageItem() { Title = "Мой Медикер", Icon = "health_insurance.png", TargetType = typeof(StartPage), Selected = true },
                new MasterPageItem() { Title = "История визитов", Icon = "health_tests.png", TargetType = typeof(AkgunHistoryPage) },
                new MasterPageItem() { Title = "Моя программа", Icon = "appointment_request.png", TargetType = typeof(ProgramsPage) },
                new MasterPageItem() { Title = "Заявка на прием", Icon = "medical_services.png", TargetType = typeof(StubPage), IsEnabled = false },
                new MasterPageItem() { Title = "Рекомендация врача", Icon = "doctor_2.png", TargetType = typeof(StubPage), IsEnabled = false },
                new MasterPageItem() { Title = "Что нового", Icon = "medical_news.png", TargetType = typeof(StubPage), IsEnabled = false },
                new MasterPageItem() { Title = "Медцентры", Icon = "global_health.png", TargetType = typeof(AllHospitalsPage) },
                new MasterPageItem() { Title = "Выход", Icon = "", TargetType = typeof(LogoutPageVm) }
            };
            SelectedMenuItem = MenuItems.First();
        }
    }
}
