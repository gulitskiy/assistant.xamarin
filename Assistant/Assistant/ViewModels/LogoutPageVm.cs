﻿using Assistant.Services;
using Assistant.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Assistant.ViewModels {
    public class LogoutPageVm : BaseVm<LogoutPage> {
        public LogoutPageVm(LogoutPage view) : base(view) {
            _authService = new AuthService();
            Application.Current.Properties.Remove("token");
            Application.Current.Properties.Remove("iin");
            Xamarin.Forms.Device.BeginInvokeOnMainThread(async () => {
                await Application.Current.SavePropertiesAsync();
                Application.Current.MainPage = new LoginPage();
            });
        }

        private readonly AuthService _authService;
    }
}
