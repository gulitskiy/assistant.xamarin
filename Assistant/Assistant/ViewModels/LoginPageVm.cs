﻿using Assistant.Services;
using Assistant.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Assistant.ViewModels {
    public class LoginPageVm : BaseVm<LoginPage> {
        public string IIN { get; set; }
        public string Password { get; set; }
        public string Code { get; set; }

        public bool _canDoLogin = true;
        public bool CanDoLogin {
            get => _canDoLogin;
            set {
                _canDoLogin = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(LoginBtnContent));
                LoginCmd.ChangeCanExecute();
            }
        }

        public string LoginBtnContent {
            get => _canDoLogin ? "Войти" : "Отправка данных...";
        }

        public Command LoginCmd { get; set; }
        public ICommand RegistrationCmd { get; set; }

        public LoginPageVm(LoginPage view) : base(view) {
            LoginCmd = new Command(async () => await DoLogin(), () => CanDoLogin);
            RegistrationCmd = new Command(async () => await DoRegistration());
            _authService = new AuthService();
        }

        async Task DoLogin() {
            if (!await Validate()) return;
            CanDoLogin = false;

            try {
                var token = await _authService.GetAccessToken(IIN, Password, Code);
                Application.Current.Properties["token"] = token;
                //Application.Current.Properties["iin"] = "111214604982";
                //Application.Current.Properties["iin"] = "900429350453";
                Application.Current.Properties["iin"] = IIN;
                await Application.Current.SavePropertiesAsync();
                await View.Navigation.PushAsync(new MainPage());
                View.Navigation.RemovePage(View);
            } catch (Exception ex) {
                await View.DisplayAlert("Ошибка входа", "Проверьте введеные данные и попробуйте еще раз.", "ОК");
            } finally {
                CanDoLogin = true;
            }
        }

        async Task<bool> Validate() {
            if (!Validator.IsValidIIN(IIN)) {
                await View.DisplayAlert("Ошибка", "Некорректный ИИН.", "ОК");
                return false;
            }
            if (string.IsNullOrEmpty(Password)) {
                await View.DisplayAlert("Ошибка", "Не указан пароль.", "ОК");
                return false;
            }
            if (!Validator.IsValidCode(Code)) {
                await View.DisplayAlert("Ошибка", "Некорректный код доступа.", "ОК");
                return false;
            }
            return true;
        }

        async Task DoRegistration() {
            await View.Navigation.PushAsync(new RegistrationPage());
            View.Navigation.RemovePage(View);
        }

        private readonly AuthService _authService;
    }
}
