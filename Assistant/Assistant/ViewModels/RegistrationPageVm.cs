﻿
using Assistant.Services;
using Assistant.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Assistant.ViewModels {
    public class RegistrationPageVm : BaseVm<RegistrationPage> {
        public Command RegistrationCmd { get; set; }
        public Command LoginCmd { get; set; }
        public string IIN { get; set; }
        public string PhoneNumber { get; set; }
        public bool _canDoRegistration = true;
        public bool CanDoRegistration {
            get => _canDoRegistration;
            set {
                _canDoRegistration = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(RegistrationBtnContent));
                RegistrationCmd.ChangeCanExecute();
            }
        }

        public string RegistrationBtnContent {
            get => _canDoRegistration ? "Запросить код и пароль" : "Отправка данных...";
        }

        public RegistrationPageVm(RegistrationPage view) : base(view) {
            RegistrationCmd = new Command(async () => await DoRegistration(), () => CanDoRegistration);
            LoginCmd = new Command(async () => await DoLogin(), () => CanDoRegistration);
            _authService = new AuthService();
        }

        async Task DoRegistration() {
            if (!await Validate()) return;

            Device.BeginInvokeOnMainThread(() => {
                CanDoRegistration = false;
            });
            try {
                await _authService.RequestAccessCodeAndPassword(IIN, PhoneNumber);
                await View.DisplayAlert("Готово", "В течении минуты на ваш номер телефона придет смс с кодом доступа и паролем, используйте их для входа.", "ОК");
                await DoLogin();
            } catch (Exception ex) {
                await View.DisplayAlert("Ошибка", "Не удалось запросить доступ по введенным вами данным. Попробуйте еще раз.", "ОК");
            } finally {
                Device.BeginInvokeOnMainThread(() => {
                    CanDoRegistration = true;
                });
            }
        }

        async Task<bool> Validate() {
            if (!Validator.IsValidIIN(IIN)) {
                await View.DisplayAlert("Ошибка", "Некорректный ИИН.", "ОК");
                return false;
            }
            if (!Validator.IsValidPhoneNumber(PhoneNumber)) {
                await View.DisplayAlert("Ошибка", "Некорректный номер телефона.", "ОК");
                return false;
            }
            return true;
        }

        async Task DoLogin() {
            await View.Navigation.PushAsync(new LoginPage());
            View.Navigation.RemovePage(View);
        }

        private readonly AuthService _authService;
    }
}
