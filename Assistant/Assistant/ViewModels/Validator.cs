﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assistant.ViewModels {
    public class Validator {
        public static bool IsValidIIN(string iin) {
            if (string.IsNullOrEmpty(iin)) return false;
            var regex = new System.Text.RegularExpressions.Regex("[0-9]{12}");
            if (regex.IsMatch(iin)) return true;
            return false;
        }

        public static bool IsValidCode(string code) {
            if (string.IsNullOrEmpty(code)) return false;
            var regex = new System.Text.RegularExpressions.Regex("[0-9]{4}");
            if (regex.IsMatch(code)) return true;
            return false;
        }

        public static bool IsValidPhoneNumber(string phoneNumber) {
            if (string.IsNullOrEmpty(phoneNumber)) return false;
            var regex = new System.Text.RegularExpressions.Regex(@"\+7[0-9]{9}");
            if (regex.IsMatch(phoneNumber)) return true;
            return false;
        }
    }
}
