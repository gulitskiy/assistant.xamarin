﻿using Assistant.Models;
using Assistant.Services;
using Assistant.Views;
using Syncfusion.SfRotator.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Assistant.ViewModels {
    public class ProgramsPageVm : BaseVm<ProgramsPage> {
        public bool StateVisibility { get; set; } = true;
        public bool ListVisibility { get; set; } = false;
        public bool IsEmpty { get; set; } = false;
        AssistantService _assistantService;
        public ObservableCollection<Program> Programs { get; set; }

        public ProgramsPageVm(ProgramsPage view, AssistantService assistantService) : base(view) {
            _assistantService = assistantService;
            Programs = new ObservableCollection<Program>();
        }

        public override Task AfterAppearing() {
            StateVisibility = true;
            ListVisibility = false;
            OnPropertyChanged(nameof(StateVisibility));
            OnPropertyChanged(nameof(ListVisibility));
            return System.Threading.Tasks.Task.Factory.StartNew(async () => {
                try {
                    var res = await _assistantService.GetProgram();
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(() => {
                        Programs.Clear();
                        foreach (var item in res) {
                            Programs.Add(item);
                        }
                        StateVisibility = false;
                        ListVisibility = Programs.Count > 0;
                        IsEmpty = Programs.Count == 0;
                        OnPropertyChanged(nameof(StateVisibility));
                        OnPropertyChanged(nameof(ListVisibility));
                        OnPropertyChanged(nameof(IsEmpty));
                    });
                } catch (WebException ex) {
                    await AuthService.Logout();
                } catch (Exception ex) {

                }
            });
        }
    }
}
