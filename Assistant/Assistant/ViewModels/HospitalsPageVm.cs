﻿using Assistant.Models;
using Assistant.Services;
using Assistant.Views;
using Syncfusion.SfRotator.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Assistant.ViewModels {
    public class HospitalsPageVm : BaseVm<HospitalsPage> {
        public bool StateVisibility { get; set; } = true;
        public bool ListVisibility { get; set; } = false;
        AssistantService _assistantService;
        public ObservableCollection<Hospital> Hospitals { get; set; }

        public HospitalsPageVm(HospitalsPage view, AssistantService assistantService) : base(view) {
            _assistantService = assistantService;
            Hospitals = new ObservableCollection<Hospital>();
        }

        public override Task AfterAppearing() {
            StateVisibility = true;
            ListVisibility = false;
            OnPropertyChanged(nameof(StateVisibility));
            OnPropertyChanged(nameof(ListVisibility));
            return System.Threading.Tasks.Task.Factory.StartNew(async () => {
                try {
                    var res = await _assistantService.GetHospitals();
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(() => {
                        Hospitals.Clear();
                        foreach (var item in res) {
                            Hospitals.Add(item);
                        }
                        StateVisibility = false;
                        ListVisibility = true;
                        OnPropertyChanged(nameof(StateVisibility));
                        OnPropertyChanged(nameof(ListVisibility));
                    });
                } catch (WebException ex) {
                    await AuthService.Logout();
                } catch (Exception ex) {

                }
            });
        }
    }
}
