﻿using Assistant.Models;
using Assistant.Services;
using Assistant.Views;
using Syncfusion.SfRotator.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Assistant.ViewModels {
    public class AkgunHospitalsPageVm : BaseVm<AkgunHospitalsPage> {
        public bool StateVisibility { get; set; } = true;
        public bool ListVisibility { get; set; } = false;
        AkgunService _akgunService;
        public ObservableCollection<AkgunHospital> Hospitals { get; set; }

        public AkgunHospitalsPageVm(AkgunHospitalsPage view, AkgunService akgunService) : base(view) {
            _akgunService = akgunService;
            Hospitals = new ObservableCollection<AkgunHospital>();
        }

        public override Task AfterAppearing() {
            StateVisibility = true;
            ListVisibility = false;
            OnPropertyChanged(nameof(StateVisibility));
            OnPropertyChanged(nameof(ListVisibility));
            return System.Threading.Tasks.Task.Factory.StartNew(async () => {
                try {
                    var res = await _akgunService.GetHospitals();
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(() => {
                        Hospitals.Clear();
                        if (res != null) {
                            foreach (var item in res) {
                                Hospitals.Add(item);
                            }
                        }
                        StateVisibility = false;
                        ListVisibility = true;
                        OnPropertyChanged(nameof(StateVisibility));
                        OnPropertyChanged(nameof(ListVisibility));
                    });
                } catch (WebException ex) {
                    await AuthService.Logout();
                } catch (Exception ex) {

                }
            });
        }
    }
}
