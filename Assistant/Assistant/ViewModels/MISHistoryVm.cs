﻿using Assistant.Models;
using Assistant.Services;
using Assistant.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Assistant.ViewModels {
    public class MISHistoryVm : BaseVm<MISHistory> {
        public bool StateVisibility { get; set; } = true;
        public bool ListVisibility { get; set; } = false;

        int _take = 15;
        int _offset = 0;

        AssistantService _assistantService;
        public ObservableCollection<History> History { get; set; }

        public MISHistoryVm(MISHistory view, AssistantService assistantService)
            : base(view) {
            _assistantService = assistantService;
            History = new ObservableCollection<History>();
        }

        public override Task AfterAppearing() {
            StateVisibility = true;
            ListVisibility = false;
            OnPropertyChanged(nameof(StateVisibility));
            OnPropertyChanged(nameof(ListVisibility));
            return System.Threading.Tasks.Task.Factory.StartNew(async () => {
                try {
                    var res = await _assistantService.GetHistory();
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(() => {
                        History.Clear();
                        foreach (var item in res) {
                            History.Add(item);
                        }
                        StateVisibility = false;
                        ListVisibility = true;
                        OnPropertyChanged(nameof(StateVisibility));
                        OnPropertyChanged(nameof(ListVisibility));
                    });
                } catch (WebException ex) {
                    await AuthService.Logout();
                } catch (Exception ex) {

                }
            });
        }
    }
}
