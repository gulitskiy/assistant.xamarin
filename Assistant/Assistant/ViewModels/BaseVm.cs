﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Assistant.ViewModels {
    public class BaseVm<TView> : INotifyPropertyChanged where TView : Page {
        public event PropertyChangedEventHandler PropertyChanged;
        public INavigation Navigation { get; set; }
        public TView View { get; set; }

        public BaseVm(TView view) {
            View = view;
            View.Appearing += View_Appearing;
        }

        private async void View_Appearing(object sender, EventArgs e) {
            await AfterAppearing();
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public virtual Task AfterAppearing() {
            return Task.FromResult(0);
        }
    }
}
