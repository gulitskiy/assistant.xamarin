﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assistant.Models {
    public class AkgunHospital {
        [JsonProperty("key")]
        public int Id { get; set; }
        [JsonProperty("value")]
        public string Name { get; set; }
        public string FullAddress { get; set; }
        public bool HasPhoneNumbers { get; set; } = false;
    }

    public class AkgunHospitalResponse {
        [JsonProperty("hastaneler")]
        public IEnumerable<AkgunHospital> Items { get; set; }
    }
}
