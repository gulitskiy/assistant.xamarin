﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assistant.Models {
    public class History {
        [JsonProperty("dcode")]
        public string DiagnosisCode { get; set; }
        [JsonProperty("date")]
        public string Date { get; set; }
        [JsonProperty("servicename")]
        public string ServiceName { get; set; }
        [JsonProperty("sum")]
        public long Sum { get; set; }
        [JsonProperty("hospital")]
        public string Hospital { get; set; }
        [JsonProperty("diagnos")]
        public string Diagnosis { get; set; }

        [JsonIgnore]
        public string ShortDate {
            get {
                if (!string.IsNullOrEmpty(Date)) {
                    DateTime date;
                    if (DateTime.TryParse(Date, out date)) {
                        return date.ToString("dd.MM.yyyy HH:mm");
                    }
                }
                return "";
            }
        }
        [JsonIgnore]
        public string SumText {
            get {
                return $"Сумма: {Sum} тг.";
            }
        }
    }
}
