﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assistant.Models {
    public class AkgunHistory {
        [JsonProperty("protokolNo")]
        public string Id { get; set; }
        [JsonProperty("dcode")]
        public string DiagnosisCode { get; set; }
        string _date { get; set; }
        [JsonProperty("vizitTarihi")]
        public string Date {
            get {
                if (!string.IsNullOrEmpty(_date)) {
                    DateTime date;
                    if (DateTime.TryParse(_date, out date)) {
                        return date.ToString("dd.MM.yyyy");
                    }
                }
                return "";
            }
            set {
                _date = value;
            }
        }
        [JsonProperty("hastaneAdi")]
        public string Hospital { get; set; }
        [JsonProperty("hastaneKodu")]
        public int HospitalId { get; set; }
        string _doctorName;
        [JsonProperty("doktorAdi")]
        public string DoctorName {
            get => $"Врач: {_doctorName}";
            set {
                _doctorName = value;
            }
        }
        [JsonProperty("doktorKodu")]
        public string DoctorId { get; set; }
        [JsonProperty("birimAdi")]
        public string ServiceName { get; set; }
        [JsonProperty("birimKodu")]
        public string ServiceId { get; set; }

        [JsonIgnore]
        public bool HasDoctorName => !string.IsNullOrEmpty(_doctorName);

        [JsonIgnore]
        public DateTime Order {
            get {
                if (!string.IsNullOrEmpty(_date)) {
                    DateTime date;
                    if (DateTime.TryParse(_date, out date)) {
                        return date;
                    }
                }
                return DateTime.Now;
            }
        }
    }

    public class AkgunHistoryResponse {
        [JsonProperty("basvurular")]
        public IEnumerable<AkgunHistory> Items { get; set; }
    }
}
