﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assistant.Models {
    public class UserInfo {
        [JsonProperty("fn")]
        public string FirstName { get; set; }
        [JsonProperty("pn")]
        public string Patronymic { get; set; }
        [JsonProperty("ln")]
        public string LastName { get; set; }
        [JsonProperty("iin")]
        public string IIN { get; set; }
        [JsonProperty("sex")]
        public string Gender { get; set; }
        [JsonProperty("birthday")]
        public string BirthDate { get; set; }
        [JsonProperty("address")]
        public string Address { get; set; }
        [JsonProperty("cardname")]
        public string CardNumber { get; set; }
        [JsonProperty("cardstatus")]
        public string CardStatus { get; set; }
        [JsonProperty("date_begin")]
        public string CardBeginDate { get; set; }
        [JsonProperty("date_end")]
        public string CardEndDate { get; set; }
        [JsonProperty("company")]
        public string CardCompany { get; set; }
        [JsonProperty("insurer")]
        public string CardInsurer { get; set; }

        [JsonIgnore]
        public string CardName => CardNumber;
        [JsonIgnore]
        public string CardDescription => $"{CardCompany}. Период страхования {CardBeginDate?.Substring(0, 10)}-{CardEndDate?.Substring(0, 10)}. {CardHolderFullName}";
        [JsonIgnore]
        public string CardHolderFullName => string
            .Join(" ", new string[] { LastName, FirstName, Patronymic }.Where(x => !string.IsNullOrEmpty(x)));
    }
}
