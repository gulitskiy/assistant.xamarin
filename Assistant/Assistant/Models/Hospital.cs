﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assistant.Models {
    public class Hospital {
        [JsonProperty("hname")]
        public string Name { get; set; }
        [JsonProperty("phone")]
        public string PhoneNumbers { get; set; }
        [JsonProperty("region")]
        public string Region { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("address")]
        public string Address { get; set; }
        [JsonProperty("hking")]
        public int Type { get; set; }

        [JsonIgnore]
        public string FullAddress => string.Join(", ", new string[] { Region, City, Address }.Where(x => !string.IsNullOrEmpty(x)));
        [JsonIgnore]
        public string PhoneNumbersString => $"Телефон: {PhoneNumbers}";
        [JsonIgnore]
        public bool HasPhoneNumbers => !string.IsNullOrEmpty(PhoneNumbers) && PhoneNumbers.Length > 1;
    }
}
