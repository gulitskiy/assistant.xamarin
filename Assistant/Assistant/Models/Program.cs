﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assistant.Models {
    public class Program {
        [JsonProperty("lName")]
        public string Name { get; set; }
        [JsonProperty("lCredit")]
        public string Credit { get; set; }
        [JsonProperty("lDebet")]
        public double? Debet { get; set; }
        [JsonProperty("lSaldo")]
        public string Saldo { get; set; }
    }
}
