﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assistant.Models {
    public class MasterPageItem {
        public string Title { get; set; }
        public string Icon { get; set; }
        public Type TargetType { get; set; }
        public bool IsEnabled { get; set; } = true;
        public bool Selected { get; set; } = false;
    }
}
