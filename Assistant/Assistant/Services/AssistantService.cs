﻿using Assistant.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Assistant.Services {
    public class AssistantService {
        public Task<IEnumerable<UserInfo>> GetUserInfo() {
            return GetUserInfo(Application.Current.Properties["iin"].ToString());
        }

        public async Task<IEnumerable<UserInfo>> GetUserInfo(string iin) {
            var request = CreateWebRequest("/api/Assistant/GetUserInfo");

            byte[] byteArray = Encoding.UTF8.GetBytes(iin);

            using (var writer = await request.GetRequestStreamAsync()) {
                await writer.WriteAsync(byteArray, 0, byteArray.Length);
            }

            using (var response = await request.GetResponseAsync() as HttpWebResponse) {
                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();
                using (var reader = new StreamReader(response.GetResponseStream())) {
                    return JsonConvert.DeserializeObject<IEnumerable<UserInfo>>(reader.ReadToEnd());
                }
            }
        }

        public Task<IEnumerable<History>> GetHistory() {
            return GetHistory(Application.Current.Properties["iin"].ToString());
        }

        public async Task<IEnumerable<History>> GetHistory(string iin) {
            var request = CreateWebRequest("/api/Assistant/GetHistory");

            byte[] byteArray = Encoding.UTF8.GetBytes(iin);

            using (var writer = await request.GetRequestStreamAsync()) {
                await writer.WriteAsync(byteArray, 0, byteArray.Length);
            }

            using (var response = await request.GetResponseAsync() as HttpWebResponse) {
                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();
                using (var reader = new StreamReader(response.GetResponseStream())) {
                    return JsonConvert.DeserializeObject<IEnumerable<History>>(reader.ReadToEnd());
                }
            }
        }


        public Task<IEnumerable<Program>> GetProgram() {
            return GetProgram(Application.Current.Properties["iin"].ToString());
        }

        public async Task<IEnumerable<Program>> GetProgram(string iin) {
            var request = CreateWebRequest("/api/Assistant/GetProgram");

            byte[] byteArray = Encoding.UTF8.GetBytes(iin);

            using (var writer = await request.GetRequestStreamAsync()) {
                await writer.WriteAsync(byteArray, 0, byteArray.Length);
            }

            using (var response = await request.GetResponseAsync() as HttpWebResponse) {
                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();
                using (var reader = new StreamReader(response.GetResponseStream())) {
                    return JsonConvert.DeserializeObject<IEnumerable<Program>>(reader.ReadToEnd());
                }
            }
        }

        public async Task<IEnumerable<Hospital>> GetHospitals() {
            var url = "/api/Assistant/GetHospitals";
            IEnumerable<Hospital> res;
            
            if (MonkeyCache.FileStore.Barrel.Current.Exists(url)
                && !MonkeyCache.FileStore.Barrel.Current.IsExpired(url)) {
                return MonkeyCache.FileStore.Barrel.Current.Get<IEnumerable<Hospital>>(url);
            }

            var request = CreateWebRequest(url);

            byte[] byteArray = Encoding.UTF8.GetBytes("");
            using (var writer = await request.GetRequestStreamAsync()) {
                await writer.WriteAsync(byteArray, 0, byteArray.Length);
            }

            using (var response = await request.GetResponseAsync() as HttpWebResponse) {
                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();
                using (var reader = new StreamReader(response.GetResponseStream())) {
                    res = JsonConvert.DeserializeObject<IEnumerable<Hospital>>(reader.ReadToEnd());
                    MonkeyCache.FileStore.Barrel.Current.Add(url, res, TimeSpan.FromDays(10));
                    return res;
                }
            }
        }

        HttpWebRequest CreateWebRequest(string path) {
            var request = WebRequest.Create($"{AppConfig.ApiHost}{path}") as HttpWebRequest;

            var token = Application.Current.Properties["token"].ToString();

            request.KeepAlive = true;
            request.Method = "POST";
            request.Timeout = 60000;
            request.ContentType = "application/json";
            request.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;
            return request;
        }
    }
}
