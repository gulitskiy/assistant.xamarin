﻿using Assistant.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Assistant.Services {
    public class AuthService {
        public async Task<string> GetAccessToken(string iin, string password, string code) {
            var request = WebRequest.Create($"{AppConfig.ApiHost}/api/Authentication/GetAccess") as HttpWebRequest;

            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            var obj = new { iin, password, accessCodTemp = code };

            var param = JsonConvert.SerializeObject(obj);
            byte[] byteArray = Encoding.UTF8.GetBytes(param);

            using (var writer = await request.GetRequestStreamAsync()) {
                await writer.WriteAsync(byteArray, 0, byteArray.Length);
            }
            using (var response = (await request.GetResponseAsync()) as HttpWebResponse) {
                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();
                using (var reader = new StreamReader(response.GetResponseStream())) {
                    string result = reader.ReadToEnd();
                    return result;
                }
            }
        }

        public async Task RequestAccessCodeAndPassword(string iin, string phoneNumber) {
            var request = WebRequest.Create($"{AppConfig.ApiHost}/api/Authentication/SendCod") as HttpWebRequest;

            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            var obj = new { iin, phoneNumber };
            var param = JsonConvert.SerializeObject(obj);
            byte[] byteArray = Encoding.UTF8.GetBytes(param);

            using (var writer = await request.GetRequestStreamAsync()) {
                await writer.WriteAsync(byteArray, 0, byteArray.Length);
            }

            var response = (await request.GetResponseAsync()) as HttpWebResponse;
            if (response.StatusCode != HttpStatusCode.OK) throw new Exception();
        }

        public static async Task Logout() {
            Application.Current.Properties.Remove("token");
            await Application.Current.SavePropertiesAsync();
            Application.Current.MainPage = new LoginPage();
        }
    }
}
