﻿using Assistant.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Assistant.Services {
    public class AkgunService {
        public Task<IEnumerable<UserInfo>> GetUserInfo() {
            return GetUserInfo(Application.Current.Properties["iin"].ToString());
        }

        public async Task<IEnumerable<UserInfo>> GetUserInfo(string iin) {
            var url = "/api/Assistant/GetUserInfo";
            IEnumerable<UserInfo> res;

            if (MonkeyCache.FileStore.Barrel.Current.Exists(url + iin)
                && !MonkeyCache.FileStore.Barrel.Current.IsExpired(url + iin)) {
                return MonkeyCache.FileStore.Barrel.Current.Get<IEnumerable<UserInfo>>(url + iin);
            }

            var request = CreateWebRequest(url);

            byte[] byteArray = Encoding.UTF8.GetBytes(iin);

            using (var writer = await request.GetRequestStreamAsync()) {
                await writer.WriteAsync(byteArray, 0, byteArray.Length);
            }

            using (var response = await request.GetResponseAsync() as HttpWebResponse) {
                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();
                using (var reader = new StreamReader(response.GetResponseStream())) {
                    res = JsonConvert.DeserializeObject<IEnumerable<UserInfo>>(reader.ReadToEnd());
                    MonkeyCache.FileStore.Barrel.Current.Add(url, res + iin, TimeSpan.FromMinutes(2));
                    return res;
                }
            }
        }

        public Task<IEnumerable<AkgunHistory>> GetHistory() {
            return GetHistory(Application.Current.Properties["iin"].ToString());
        }

        public async Task<IEnumerable<AkgunHistory>> GetHistory(string iin) {
            var url = "/api/Akgun/GetOkuBasvuruBilgi";
            IEnumerable<AkgunHistory> res;

            if (MonkeyCache.FileStore.Barrel.Current.Exists(url + iin)
                && !MonkeyCache.FileStore.Barrel.Current.IsExpired(url + iin)) {
                return MonkeyCache.FileStore.Barrel.Current.Get<IEnumerable<AkgunHistory>>(url + iin);
            }

            var request = CreateWebRequest(url);

            byte[] byteArray = Encoding.UTF8.GetBytes(iin);

            using (var writer = await request.GetRequestStreamAsync()) {
                await writer.WriteAsync(byteArray, 0, byteArray.Length);
            }

            using (var response = await request.GetResponseAsync() as HttpWebResponse) {
                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();
                using (var reader = new StreamReader(response.GetResponseStream())) {
                    res = JsonConvert
                        .DeserializeObject<AkgunHistoryResponse>(reader.ReadToEnd())
                        .Items;
                    if(res != null && res.Count() > 0) {
                        res = res.OrderByDescending(x => x.Order);
                    }
                    MonkeyCache.FileStore.Barrel.Current.Add(url, res + iin, TimeSpan.FromMinutes(2));
                    return res;
                }
            }
        }


        public Task<IEnumerable<Program>> GetProgram() {
            return GetProgram(Application.Current.Properties["iin"].ToString());
        }

        public async Task<IEnumerable<Program>> GetProgram(string iin) {
            var url = "/api/Assistant/GetProgram";

            IEnumerable<Program> res;

            if(MonkeyCache.FileStore.Barrel.Current.Exists(url + iin)
                && !MonkeyCache.FileStore.Barrel.Current.IsExpired(url + iin)) {
                return MonkeyCache.FileStore.Barrel.Current.Get<IEnumerable<Program>>(url + iin);
            }

            var request = CreateWebRequest(url);

            byte[] byteArray = Encoding.UTF8.GetBytes(iin);

            using (var writer = await request.GetRequestStreamAsync()) {
                await writer.WriteAsync(byteArray, 0, byteArray.Length);
            }

            using (var response = await request.GetResponseAsync() as HttpWebResponse) {
                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();
                using (var reader = new StreamReader(response.GetResponseStream())) {
                    res = JsonConvert.DeserializeObject<IEnumerable<Program>>(reader.ReadToEnd());
                    MonkeyCache.FileStore.Barrel.Current.Add(url, res + iin, TimeSpan.FromMinutes(2));
                    return res;
                }
            }
        }

        public Task<IEnumerable<AkgunHospital>> GetHospitals() {
            var res = new List<AkgunHospital>();

            res.Add(new AkgunHospital() {
                Name = "Медицинский центр \"Медикер Жайык\" (Атырау) (7)",
                FullAddress = "Город: Атырау, ул. Севастополь, 10А"
            });
            res.Add(new AkgunHospital() {
                Name = "Медицинский центр \"Медикер Каспий\" (Актау) (12)",
                FullAddress = "Город: Актау, 26 мкр., здание №17/1 (рядом с жилым домом №14)"
            });
            res.Add(new AkgunHospital() {
                Name = "Медицинский центр \"Медикер\" (Жанаозен) (8)",
                FullAddress = "Город: Жанаозен, мкр. Самал, здание 39А"
            });
            res.Add(new AkgunHospital() {
                Name = "Медицинский центр \"Медикер\" (Изумрудный) (3)",
                FullAddress = "Город: Астана,  ул. Кунаева, 8Б"
            });
            res.Add(new AkgunHospital() {
                Name = "Медицинский центр \"Медикер\" (Лукойл) (6)",
                FullAddress = "Город: Астана, пр. Кабанбай батыра 17, блок Б"
            });
            res.Add(new AkgunHospital() {
                Name = "Медицинский центр \"Медикер ЮК\" (Шымкент) (14)",
                FullAddress = "Город: Шымкент, ЮКО, город Шымкент, район Каратау, Жилой Массив Нурсат, здание 640А"
            });
            res.Add(new AkgunHospital() {
                Name = "Медицинский центр \"Медикер\" г.Алматы (16)",
                FullAddress = "Город: Алмата, ул. Сатпаева 16Б"
            });
            res.Add(new AkgunHospital() {
                Name = "Медицинский центр \"Медикер Аксай\" (Аксай) (11)",
                FullAddress = "Город: Аксай, 2 мкр, дом.1/3"
            });
            res.Add(new AkgunHospital() {
                Name = "Медицинский центр \"Медикер Аксай\" (Уральск) (10)",
                FullAddress = "Город: Уральск, ул. Ж. Молдагалиева, д. 23 Н"
            });
            res.Add(new AkgunHospital() {
                Name = "\"Медцентр ЭКСПО\" (20)",
                FullAddress = "Город: Астана, пр. Кабанбай батыра, 60 А/19"
            });
            res.Add(new AkgunHospital() {
                Name = "Педиатрический медицинский центр \"Медикер\" (4)",
                FullAddress = "Город: Астана, ул. Ташенова, д. 20"
            });
            res.Add(new AkgunHospital() {
                Name = "Врачебная амбулатория Центра восточной медицины (5)",
                FullAddress = "Город: Астана, ул Кургальжинское шоссе, 4/2"
            });
            res.Add(new AkgunHospital() {
                Name = "ТОО \"Многопрофильный медицинский центр Мейiрiм\" (2)",
                FullAddress = "Город: Астана, ул. Сыганак, д. 1"
            });
            res.Add(new AkgunHospital() {
                Name = "ТОО \"Филиал медицинские центры \"Медикер - Кульсай\"(1)",
                FullAddress = "Город: Астана, ул Кургальжинское шоссе, 4/1"
            });
            res.Add(new AkgunHospital() {
                Name = "МЦ \"Медикер - Илек\" г.Актобе (15)",
                FullAddress = "Город: Актобе, ул. Аз - Наурыз, д. 5"
            });
            res.Add(new AkgunHospital() {
                Name = "\"Медикер 32\", г.Астана",
                FullAddress = "Город: Астана, ул. Ташенова, 20 "
            });
            res.Add(new AkgunHospital() {
                Name = "\"Медикер Алатау\", г.Алматы",
                FullAddress = "Город: Алматы, мкр. Орбита-2, ул. Навои, 310"
            });

            return Task.FromResult<IEnumerable<AkgunHospital>>(res);

            /*var request = CreateWebRequest("/api/Akgun/GetOkuHastaneListesi");

            byte[] byteArray = Encoding.UTF8.GetBytes("");
            using (var writer = await request.GetRequestStreamAsync()) {
                await writer.WriteAsync(byteArray, 0, byteArray.Length);
            }

            using (var response = await request.GetResponseAsync() as HttpWebResponse) {
                if (response.StatusCode != HttpStatusCode.OK) throw new Exception();
                using (var reader = new StreamReader(response.GetResponseStream())) {
                    return JsonConvert
                        .DeserializeObject<AkgunHospitalResponse>(reader.ReadToEnd())
                        .Items;
                }
            }*/
        }

        HttpWebRequest CreateWebRequest(string path) {
            var request = WebRequest.Create($"{AppConfig.ApiHost}{path}") as HttpWebRequest;

            var token = Application.Current.Properties["token"].ToString();

            request.KeepAlive = true;
            request.Method = "POST";
            request.Timeout = 60000;
            request.ContentType = "application/json";
            request.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;
            return request;
        }
    }
}
