using Assistant.Views;
using MonkeyCache.FileStore;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Assistant {
    public partial class App : Application {
        public App() {
            //Register Syncfusion license
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NTgwN0AzMTM2MmUzMjJlMzBBYUJXdVZMUklPWnBZeWlqc1JyN0NHM2FZWWEwTHgzT0dDOHk2cnpzL1FJPQ==");
            InitializeComponent();

            var mainPageType = typeof(LoginPage);

            if (Application.Current.Properties.Keys.Contains("token")) {
                var token = Application.Current.Properties["token"]?.ToString();
                if (!string.IsNullOrEmpty(token)) {
                    mainPageType = typeof(MainPage);
                }
            }
            Barrel.ApplicationId = "kz.mediker.assistant";
            MainPage = new NavigationPage((Page)Activator.CreateInstance(mainPageType));
        }

        protected override void OnStart() {
            // Handle when your app starts
        }

        protected override void OnSleep() {
            // Handle when your app sleeps
        }

        protected override void OnResume() {
            // Handle when your app resumes
        }
    }
}
