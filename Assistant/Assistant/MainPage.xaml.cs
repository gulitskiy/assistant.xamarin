﻿using Assistant.Models;
using Assistant.ViewModels;
using Assistant.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Assistant {
    public partial class MainPage : MasterDetailPage {
        public MainPage() {
            InitializeComponent();
            BindingContext = new MainPageVm(this);
        }

        private void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e) {
            var item = (MasterPageItem)e.SelectedItem;
            Type page = item.TargetType;
            if (page == typeof(LogoutPageVm)) {
                Application.Current.MainPage = new LoginPage();
            } else {
                Detail = new NavigationPage((Page)Activator.CreateInstance(page));
            }
            IsPresented = false;
        }
    }
}
