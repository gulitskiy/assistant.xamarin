﻿using Assistant.ViewModels;
using Syncfusion.ListView.XForms;
using Syncfusion.SfRotator.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Assistant.Views {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StartPage : ContentPage {

        public StartPage() {
            InitializeComponent();
            BindingContext = new StartPageVm(this, new Services.AssistantService());
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e) {
            Device.OpenUri(new Uri("http://mediker.kz"));
        }
    }
}