﻿using Assistant.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
//using System.Web.Script.Serialization;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Assistant.Views {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LogoutPage : ContentPage {
        public LogoutPage() {
            InitializeComponent();
            BindingContext = new LogoutPageVm(this);
        }
    }
}