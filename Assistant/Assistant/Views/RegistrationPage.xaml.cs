﻿using Assistant.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Assistant.Views {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationPage : ContentPage {
        public RegistrationPage() {
            InitializeComponent();
            BindingContext = new RegistrationPageVm(this);
        }
    }
}