﻿using Assistant.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Assistant.Views {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MISHistory : ContentPage {
        public MISHistory() {
            InitializeComponent();
            BindingContext = new MISHistoryVm(this, new Services.AssistantService());
        }
    }
}