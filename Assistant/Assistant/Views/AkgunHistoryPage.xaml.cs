﻿using Assistant.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Assistant.Views {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AkgunHistoryPage : ContentPage {
        public AkgunHistoryPage() {
            InitializeComponent();
            BindingContext = new AkgunHistoryPageVm(this, new Services.AkgunService());
        }
    }
}