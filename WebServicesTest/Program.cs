﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebServicesTest
{
    class Program
    {
        static void Main(string[] args)
        {

            //History history = new History();
            //history= history.GetHistory(v.GetAccess());

            //UserInfo ui = new UserInfo();
            //ui=ui.GetUserInfo(v.GetAccess());
            SessionToken v = new SessionToken();
            List<InsProgram> inp = new List<InsProgram>();
            InsProgram inp4meth = new InsProgram();
            inp = inp4meth.GetProgram(v.GetAccess("700826300285", "XEIUmtNq", "7536"), "571207401604");

            Console.ReadKey();
        }
    }

    public class History
    {
        public string Hdcode { get; set; }
        public DateTime Hdate { get; set; }
        public string Hservicename { get; set; }
        public int Hsum { get; set; }
        public string Hhospital { get; set; }
        public string Hdiagnos { get; set; }

        public History GetHistory(string token)
        {
            var request = WebRequest.Create("http://172.18.50.184:80/api/Assistant/GetHistory") as HttpWebRequest;

            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            request.Headers.Add("Authorization","Bearer " + token);
            string iin = "571207401604";

            byte[] byteArray = Encoding.UTF8.GetBytes(iin);

            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }

                //var response = request.GetResponse() as HttpWebResponse;

                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        string z = reader.ReadToEnd();
                        History result = JsonConvert.DeserializeObject <History> (reader.ReadToEnd());
                        return result;
                    }
                }

            }
            catch (WebException ex)
            {
                return null;
            }
        }

    }

    #region class SessionToken
/// <summary>
/// Содержит метод для получения Токена
/// </summary>
    public class SessionToken
    {
        /// <summary>
        /// Метод GetAccess возвращает строку Токена
        /// </summary>
        /// <param name="iin"></param>
        /// <param name="password"></param>
        /// <param name="accessCodTemp"></param>
        /// <returns></returns>
        public string GetAccess(string iin, string password, string accessCodTemp)
        {
            var request = WebRequest.Create("http://172.18.50.184:80/api/Authentication/GetAccess") as HttpWebRequest;

            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            //var obj = new { iin = "700826300285", password = "XEIUmtNq", accessCodTemp = "7536" };
            var obj = new { iin, password, accessCodTemp };

            var param = JsonConvert.SerializeObject(obj);
            byte[] byteArray = Encoding.UTF8.GetBytes(param);

            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        string result = reader.ReadToEnd();
                        return result;
                    }
                }

            }
            catch (WebException ex)
            {
                return "";
            }

        }
        /// <summary>
        /// SendCod отправляет СМС на указанные номер телефона
        /// в формате +77ххххххххх
        /// для ИНН клиента
        /// Этот метод ничего не возвращает
        /// </summary>
        /// <param name="iin"></param>
        /// <param name="phoneNumber"></param>
        static void SendCode(string iin, string phoneNumber)
        {
            var request = WebRequest.Create("http://172.18.50.184:80/api/Authentication/SendCod") as HttpWebRequest;

            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            //var obj = new { iin = "700826300285", phoneNumber = "+77019991224" };
            var obj = new { iin, phoneNumber };
            var param = JsonConvert.SerializeObject(obj);
            byte[] byteArray = Encoding.UTF8.GetBytes(param);
            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }

                var response = request.GetResponse() as HttpWebResponse;
            }
            catch (WebException ex)
            {

            }

        }
    }
    #endregion


    public class UserInfo
    {
        public string Hdcode { get; set; }
        public DateTime Hdate { get; set; }
        public string Hservicename { get; set; }
        public int Hsum { get; set; }
        public string Hhospital { get; set; }
        public string Hdiagnos { get; set; }

        public UserInfo GetUserInfo (string token)
        {
            var request = WebRequest.Create("http://172.18.50.184:80/api/Assistant/GetUserInfo?inn=571207401604") as HttpWebRequest;

            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            request.Headers.Add("Authorization", "Bearer " + token);
            string iin = "571207401604";

            //var param = JsonConvert.SerializeObject(obj);
            byte[] byteArray = Encoding.UTF8.GetBytes(iin);

            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }

                //var response = request.GetResponse() as HttpWebResponse;

                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        string z = reader.ReadToEnd(); //Здесь хочу увидеть не пустой JSON
                        UserInfo result = JsonConvert.DeserializeObject<UserInfo>(reader.ReadToEnd());
                        return result;
                    }
                }

            }
            catch (WebException ex)
            {
                return null;
            }
        }

    }

    #region class InsProgram
    /// <summary>
    /// Формирует список параметров программы страхования
    /// Встроенный метод возвращает List j, обычно 1 - 3 строки
    /// </summary>

    public class InsProgram
    {
        public string lName { get; set; }
        public string lCredit { get; set; }
        public string lDebet { get; set; }
        public string lSaldo { get; set; }

        public List<InsProgram> GetProgram(string token, string iin)
        {
            var request = WebRequest.Create("http://172.18.50.184:80/api/Assistant/GetProgram") as HttpWebRequest;
            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.Headers.Add("Authorization", "Bearer " + token);
            byte[] byteArray = Encoding.UTF8.GetBytes(iin);
            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        List<InsProgram> result = JsonConvert.DeserializeObject<List<InsProgram>>(reader.ReadToEnd());
                        return result;
                    }
                }
            }
            catch (WebException ex)
            {
                return null;
            }
        }
    }
    #endregion

}
